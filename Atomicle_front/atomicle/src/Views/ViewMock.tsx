import React, {Component, useState} from 'react';
import {CKEditor} from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import {OBJModel} from 'react-3d-viewer';

const ViewMock = () => {


    let cameraPosition = {
        x:150,
        y:300,
        z:350
      }

    const [addData, setVal] = useState('');

    const handleChange = (e,editor) => {
        const data = editor.getData();
        setVal(data);
    };

    return(
        <div className='viewmock-container'>
            <CKEditor editor={ClassicEditor} data={addData} onChange={handleChange} />

            <OBJModel src="./indoor_plant_02.obj" texPath=""/>
        </div>
    );
};

export default ViewMock;