import '../css/Home.css';
import '../css/style.css';

const Home = (props: {userId: string}) => {

    return(
        <>
            <div className='home-container'>
                <header>
                <div>
                    <h1>Welcome to ATOMICLE</h1>
                    <p>
                        {props.userId ? 'Explore top-quality international chemistry database, take part in research activities and share results with the whole world.' : 'You are not logged in'}
                    </p>
                </div>
            </header>
            <div>
                <h2>Browse</h2>
            </div>
            <section className="home-content">
                

            </section>
            </div>
        </>
    );
};

export default Home;