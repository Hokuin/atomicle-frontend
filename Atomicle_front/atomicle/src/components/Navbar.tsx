import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { SidebarData } from './SidebarData';
import '../css/Navbar.css';
import { IconContext } from 'react-icons';
import logo from '../images/logo.svg';
import * as FaIcons from 'react-icons/fa';

const Navbar = (props: {userId: string, setUserId: (userId: string) => void}) => {

  const logout = async() =>
  {
    await fetch(process.env.REACT_APP_API + 'logout',
        {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: 'include'
        });
    props.setUserId('');
  };

  return (
    <div className='nav-menu'>
      <IconContext.Provider value={{ color: '#00657e' } }>
        
        <nav>
          <ul className='nav-menu-items'>
            <Link to="/">
              <img className='nav-logo' src={logo} alt='atomicle_logo'></img>
              </Link>
            {SidebarData.map((item, index) => {
              return (
                <li key={index} className={item.cName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span className='nav-position-title'>{item.title}</span>
                  </Link>
                </li>
              );
            })}
            <li className="nav-text">
                <Link to="/login" onClick={logout}>
                  <FaIcons.FaPowerOff/>
                  <span className='nav-position-title'>Logout</span>
                </Link>
              </li>
          </ul>
        </nav>
      </IconContext.Provider>
    </div>
  );

};

export default Navbar;