import React from 'react';
import * as FaIcons from 'react-icons/fa';

export const SidebarData = [
    {
        title: 'Search',
        path: "/search",
        icon: <FaIcons.FaSearch />,
        cName: 'nav-text'
    },
    {
        title: 'Account',
        path: "/account",
        icon: <FaIcons.FaUserCircle />,
        cName: 'nav-text'
    },
    {
        title: 'Resources',
        path: "/resources",
        icon: <FaIcons.FaTable />,
        cName: 'nav-text'
    },
    {
        title: 'Articles',
        path: "/articles",
        icon: <FaIcons.FaFileAlt />,
        cName: 'nav-text'
    },
    {
        title: 'Research',
        path: "/research",
        icon: <FaIcons.FaFlask />,
        cName: 'nav-text'
    },
    {
        title: 'Discussions',
        path: "/forum",
        icon: <FaIcons.FaComments />,
        cName: 'nav-text'
    },
    {
        title: 'Community',
        path: "/community",
        icon: <FaIcons.FaUserFriends />,
        cName: 'nav-text'
    }/*,
    {
        title: 'Logout',
        path: "/login",
        icon: <FaIcons.FaPowerOff/>,
        cName: 'nav-text'
    }*/
]